import React, { useState, useEffect } from 'react'
import './App.css'
import Board from './components/Board/Board'
import ToolBar from './components/GameSettingsBar/GameSettingsBar'
import axios from 'axios'

function App() {
	const [dimension, setDimension] = useState(0)
	const [color, setColor] = useState('bright')
	const [shape, setShape] = useState('circle')
	const [game, setGame] = useState([])
	const [dimensionArr, setDimensionArr] = useState([])
	const [isNewGame, setIsNewGame] = useState(true)
	// sets old game on mount
	useEffect(()=>{
		axios.get('http://localhost:5000/api/Checkers').then((res,err)=>{
			if (err) return
			if(res.data.data && res.data.data.game && Array.isArray(res.data.data.game)) {
				setIsNewGame(false)
				setDimension(Math.sqrt(res.data.data.game.length))
				setGame(res.data.data.game)
			}
		})
	},[])

	// inits game state
	useEffect(() => {
		if (dimension<2) return
		const dimensionArr = [...Array(dimension).keys()]
		setDimensionArr(dimensionArr)
		// skips setting init game if isNotNewGame
		if(isNewGame) {
			const game = [...Array(dimension ** 2).fill(null)]
			for (let i = 0; i < dimension * 2; i++) {
				game[i] = 'p1'
				game[game.length - 1 - i] = 'p2'
			}
			setGame(game)
		}
	}, [dimension, isNewGame])

	const toggleColor = (e) => {
		setColor(e.target.value)
	}
	const toggleShape = (e) => {
		setShape(e.target.value)
	}
	const startNewGame = (dim) => {
		if (isNaN(dim) || dim<0) {
			setDimension(0)
		} else {
			setDimension(dim)
		}
	}

	return (
		<div className="App">
			<ToolBar dimension={dimension} color={color} shape={shape} onShapeChange={toggleShape} onColorChange={toggleColor} onChangeHandler={e => startNewGame(parseInt(e.target.value))}></ToolBar>
			<Board dimension={dimension} setDimension={setDimension} setDimensionArr={setDimensionArr} setIsNewGame={setIsNewGame} game={game} setGame={setGame} dimensionArr={dimensionArr} color={color} shape={shape}></Board>
		</div>
	);
}

export default App;

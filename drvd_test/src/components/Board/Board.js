/* eslint-disable react/prop-types */
import React, { useState } from 'react'
import axios from 'axios'
import './Board.css'

function Board({ dimension, setDimension, setDimensionArr, isNewGame, dimensionArr, game, setGame, setIsNewGame, color, shape }) {
	const [player, setPlayer] = useState('p1')
	const [activePiece, setActivePiece] = useState(null)
	const [suggestedMoveL, setSuggestedMoveL] = useState(null)
	const [suggestedMoveR, setSuggestedMoveR] = useState(null)

	const pieceMove = (num) => {
		if (!activePiece || game[num] === player) {
			// new move or new active piece
			readyToMove(num)
		} else if (!game[num] && (num===suggestedMoveL || num===suggestedMoveR)) {
			[game[num], game[activePiece]] = [game[activePiece], game[num]]
			setGame(game)
			setActivePiece(null)
			setSuggestedMoveL(null)
			setSuggestedMoveR(null)
			let nextPlayer = (game[num]==='p1') ? 'p2' : 'p1'
			setPlayer(nextPlayer)
		}
	}
	const readyToMove = (num) => {
		setActivePiece(num)
		let suggested1 = (game[num] === 'p1') ? (num+dimension+2) : (num-dimension+2)
		let suggested2 = (game[num] === 'p1') ? (num+dimension-2) : (num-dimension-2)
		suggested1 = (game[suggested1] || (num%dimension >= dimension-2)) ? null : suggested1
		suggested2 = (game[suggested2] || (num%dimension < 2)) ? null : suggested2
		setSuggestedMoveL(suggested1)
		setSuggestedMoveR(suggested2)
	}
	const saveGame = async () => {
		try {
			await axios({
				method: (isNewGame) ? 'POST' : 'PUT',
				url: 'http://localhost:5000/api/Checkers',
				data: {
					game,

				}
			})
		} catch(err) {
			console.log(err)
		}
	}
	const resetGame = async () => {
		try {
			await axios({
				method: 'DELETE',
				url: 'http://localhost:5000/api/Checkers'
			})
			setDimension(0)
			setGame([])
			setDimensionArr([])
			setIsNewGame(true)
		} catch(err) {
			console.log(err)
		}
	}
	return (
		<article>
		{dimension>0 && <section>
			<input type="button" value="Save" onClick={saveGame}/>
			<input type="button" value="Reset" onClick={resetGame}/>
		</section>}
		<div className="board">
			{dimensionArr.map((c) => {
				return (
					<div key={c} className={`col ${c % 2 === 0 ? 'starts-black' : 'starts-white'}`}>
						{dimensionArr.map(r => {
							let arrLen = dimensionArr.length
							let blockNumb = (c * arrLen) + r

							return (
								<button onClick={() => pieceMove(blockNumb)} key={r} className={`${(activePiece===blockNumb)? 'active-border':''} block`}>
									{(game[blockNumb]==='p1') && <span className={`${color === 'bright' ? 'red' : 'light-red'} ${shape === 'circle' ? 'circle' : 'triangle'}`}></span>}
									{(blockNumb === suggestedMoveL || blockNumb === suggestedMoveR) && <span className={`suggested circle`}></span>}
									{(game[blockNumb]==='p2') && <span className={`${color === 'bright' ? 'black' : 'mint'} ${shape === 'circle' ? 'circle' : 'triangle'}`}></span>}
								</button>
							)
						})}
					</div>
				)
			})}
		</div>
		</article>
	)
}

export default Board

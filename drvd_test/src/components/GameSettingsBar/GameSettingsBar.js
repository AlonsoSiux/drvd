/* eslint-disable react/prop-types */
import React from 'react'
import './GameSettingsBar.css'
function ToolBar({ dimension, onChangeHandler, color, onColorChange, shape, onShapeChange }) {
	return (
		<header className="tool-bar">
			<input type="number" value={dimension} onChange={onChangeHandler}></input>
			<section className="radio-grp">
				<label>
					<input
						type="radio"
						value="bright"
						name="colors"
						checked={color === "bright"}
						onChange={onColorChange}
					/>
					<span>Bright</span>
				</label>
				<label>
					<input
						type="radio"
						value="pastel"
						name="colors"
						checked={color === "pastel"}
						onChange={onColorChange}
					/>
					<span>Pastel</span>
				</label>
			</section>
			<section className="radio-grp">
				<label>
					<input
						type="radio"
						value="circle"
						name="shapes"
						checked={shape === "circle"}
						onChange={onShapeChange}
					/>
					<span>Circle</span>
				</label>
				<label>
					<input
						type="radio"
						value="triangle"
						name="shapes"
						checked={shape === "triangle"}
						onChange={onShapeChange}
					/>
					<span>Triangle</span>
				</label>
			</section>
		</header>
	)
}

export default ToolBar

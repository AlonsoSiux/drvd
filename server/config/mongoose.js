const mongoose = require('mongoose')
mongoose.set('useCreateIndex', true)
const MONGO_URL = 'mongodb://localhost/drvd_test'

mongoose.connect(MONGO_URL, {useFindAndModify: false, useUnifiedTopology: true, retryWrites: false, useNewUrlParser: true}, (err) => {
  if (err) throw err
  console.log('mongoose successfully connected to ', MONGO_URL)
})

module.exports = mongoose

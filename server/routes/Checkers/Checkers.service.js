let Checkers = require('./Checkers.model')

const getCheckersGame = async function () {
	return await Checkers.findOne({})
}

const newCheckersGame = async function (game) {
	return await Checkers.findOneAndUpdate({}, {game}, {upsert:true})
}

const updateCheckersGame = async function (game) {
	return await Checkers.findOneAndUpdate({}, {game}, {upsert:true})
}

const deleteCheckersGame = async function () {
	return await Checkers.findOneAndDelete({})
}

module.exports = {
	getCheckersGame,
	newCheckersGame,
	updateCheckersGame,
	deleteCheckersGame
}

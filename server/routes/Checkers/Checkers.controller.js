const express = require('express')
const checkers = new express.Router()
const {getCheckersGame, newCheckersGame, updateCheckersGame, deleteCheckersGame} = require('./Checkers.service')

checkers.get('/', async (req, res)=>{
	const game = await getCheckersGame()
	res.status(200).json({message:'saved game retrieved', data: game})
})
checkers.post('/',async (req, res)=>{
	try {
		const {game} = req.body
		if (!game) return res.status(400).json({message:'missing game state'})
		const gameSaved = await newCheckersGame(game)
		return res.status(200).json({message:'saved game', data: gameSaved})
	} catch(err) {
		console.log(err)
		return res.status(500).json({message:'server error'})
	}
})
checkers.put('/',async (req, res)=>{
	try {
		const {game} = req.body
		if (!game) return res.status(400).json({message:'missing game state'})
		const gameSaved = await updateCheckersGame(game)
		return res.status(200).json({message:'saved game', data: gameSaved})
	} catch(err) {
		console.log(err)
		return res.status(500).json({message:'server error'})
	}
})

checkers.delete('/',async (req, res)=>{
	try {
		const gameDeleted = await deleteCheckersGame()
		return res.status(200).json({message:'deleted game', data: gameDeleted})
	} catch(err) {
		console.log(err)
		return res.status(500).json({message:'server error'})
	}
})

module.exports = checkers

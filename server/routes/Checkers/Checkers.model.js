const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Checkers = new Schema({
	game: {
		type: Array,
		default: []
	},
});

module.exports = mongoose.model('Checkers', Checkers);
